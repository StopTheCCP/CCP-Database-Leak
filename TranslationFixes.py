import os
import re

inputFileRange = [*range(1, 41)]  # all files from 1-40
inputFileBaseName = 'shanghai-ccp-member'
inputDir = f'Data/TranslatedPartials'.replace('/', os.path.sep)
outputDir = f'Data/FixedTranslatedPartials'.replace('/', os.path.sep)


def ApplyNullFormatting(inputRow) -> str:
    formattedRow = inputRow
    if ',"null"' in inputRow:
        formattedRow = formattedRow.replace(',"null"',',null')
    return formattedRow


def ReplaceTranslations(inputRow:str) -> str:
    formattedRow = inputRow
    if ',"Han nationality"' in inputRow:
        formattedRow = formattedRow.replace(',"Han nationality"',',"Han"')
    if ',"male"' in inputRow:
        formattedRow = formattedRow.replace(',"male"',',"Male"')
    if ',"the University"' in inputRow:
        formattedRow = formattedRow.replace(',"the University"',',"University"')
    if ',"Junior college"' in inputRow:
        formattedRow = formattedRow.replace(',"Junior college"',',"Junior College"')
    if ',"normal high school"' in inputRow:
        formattedRow = formattedRow.replace(',"normal high school"',',"High School"')
    if ',"junior high school"' in inputRow:
        formattedRow = formattedRow.replace(',"junior high school"',',"Junior High"')
    if ',"Master student"' in inputRow:
        formattedRow = formattedRow.replace(',"Master student"',',"Master\'s Degree"')
    if ',"PhD student"' in inputRow:
        formattedRow = formattedRow.replace(',"PhD student"',',"PhD"')
    return formattedRow


def CleanupOutputDirectory(outputDir):
    for (directoryPath, subdirectoryList, fileList) in os.walk(outputDir):
        for fileNameSingle in fileList:
            if fileNameSingle.endswith('.csv'):
                os.remove(os.path.join(outputDir, fileNameSingle))


def PerformFixes(inputFilePath, outputFilePath):
    with open(outputFilePath, 'w', encoding='utf-8') as outputFile:
        with open(inputFilePath, 'r', encoding='utf-8') as inputFile:
            n=0
            for row in inputFile:
                n += 1
                fixedRow = ApplyNullFormatting(row)
                fixedRow = ReplaceTranslations(fixedRow)

                if n % 1000 == 0:
                    print(f'Processing row {n}')
                # if row != fixedRow:
                #     print(row)
                #     print(fixedRow)
                outputFile.write(fixedRow)


if __name__ == '__main__':
    if not os.path.exists(outputDir):
        os.mkdir(outputDir)
    CleanupOutputDirectory(outputDir)

    for fileNumber in inputFileRange:
        fileName = f'{inputFileBaseName}_{fileNumber}.csv'
        inputFilePath = os.path.join(inputDir, fileName)
        outputFilePath = os.path.join(outputDir, fileName)
        PerformFixes(inputFilePath, outputFilePath)