import os
import random
import time
import csv
import requests
import json


# inputFileRange = [*range(1, 41)]  # all files from 1-40
inputFileRange = [1,2]  # specific files
inputFileBaseName = 'shanghai-ccp-member'
inputDir = f'Data/UntranslatedPartials'.replace('/', os.path.sep)
outputDir = f'Data/TranslatedPartials'.replace('/', os.path.sep)
maxRetries = 5
urlSuffixes = ['us', 'com', 'ca']
fieldNames = ['id', 'name', 'sex', 'ethnicity', 'hometown', 'organization',
              'id_card_num', 'address', 'mobile_num', 'phone_num', 'education']
emptyRow = {'id': None, 'name': None, 'sex': None, 'ethnicity': None, 'hometown': None, 'organization': None,
            'id_card_num': None, 'address': None, 'mobile_num': None, 'phone_num': None, 'education': None}
lastCnRow = {'id': None, 'name': None, 'sex': None, 'ethnicity': None, 'hometown': None, 'organization': None,
            'id_card_num': None, 'address': None, 'mobile_num': None, 'phone_num': None, 'education': None}
lastEnRow = {'id': None, 'name': None, 'sex': None, 'ethnicity': None, 'hometown': None, 'organization': None,
            'id_card_num': None, 'address': None, 'mobile_num': None, 'phone_num': None, 'education': None}

translateUrl = 'https://clients5.google.com/translate_a/t?client=dict-chrome-ex&sl=zh-CN&tl=en&ie=UTF-8&oe=UTF-8&q='

cacheTranslations = {}

def GetTimeString() -> str:
    t = time.localtime()
    return time.strftime('%H:%M:%S', t)


def ProcessFile(inputFilePath: str, outputFilePath: str):
    with open(outputFilePath, 'w', encoding='utf-8', newline='') as outputFile:
        outputWriter = csv.DictWriter(outputFile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL, fieldnames=fieldNames)
        with open(inputFilePath, 'r', encoding='utf-8', newline='') as inputFile:
            inputreader = csv.DictReader(
                inputFile, delimiter=',', quotechar='"', fieldnames=fieldNames)
            n = 0
            for line in inputreader:
                n += 1
                if n % 1000 == 0:
                    currentTime = GetTimeString()
                    print(f'[{currentTime}] Processing row {n}')
                translatedValues = TranslateCnToEn(line)
                outputWriter.writerow(translatedValues)


def TranslateCnToEn(textCnRow: dict, retries: int = 0) -> dict:
    global lastCnRow
    global lastEnRow
    textCn = []
    textEn = emptyRow.copy()
    textEn['id'] = textCnRow['id']
    for fieldName, value in textCnRow.items():
        if fieldName == 'id':
            pass
        elif value == 'null':
            textEn[fieldName] = 'null'
        elif value.isdecimal():
            textEn[fieldName] = value
        elif value == '#':
            textEn[fieldName] = '#'
        elif value == lastCnRow[fieldName]:
            textEn[fieldName] = lastEnRow[fieldName]
        elif value in cacheTranslations:
            textEn[fieldName] = cacheTranslations[value]
        else:
            textCn.append(value)

    try:
        n = 0
        for fieldName, value in textEn.items():
            if value:
                pass
            else:
                cnVal = textCn[n]
                results = requests.get(translateUrl+cnVal)
                translations = results.json()
                enVal = ''
                for block in translations['sentences']:
                    if 'trans' in block:
                        enVal += block['trans']
                textEn[fieldName] = enVal
                n += 1
                if fieldName in ['sex', 'ethnicity', 'hometown', 'organization','education']:
                    cacheTranslations[cnVal] = enVal

        lastCnRow = textCnRow
        lastEnRow = textEn
    except Exception as e:
        print(e)
        for fieldName, value in textEn.items():
            if value:
                pass
            else:
                textEn[fieldName] = 'Error Translating Record'
    return textEn


if __name__ == '__main__':
    time0 = time.time()
    if not os.path.exists(outputDir):
        os.mkdir(outputDir)

    for fileNumber in inputFileRange:
        fileName = f'{inputFileBaseName}_{fileNumber}.csv'
        inputFilePath = os.path.join(inputDir, fileName)
        outputFilePath = os.path.join(outputDir, fileName)

        # TODO: handle this better, but currently it's best to not overwrite anything and yell at user
        if os.path.exists(outputFilePath):
            raise RuntimeError(f'Output file already exists at {outputFilePath}') from None

        currentTime = GetTimeString()
        print(f'[{currentTime}] Processing File: {inputFilePath}')
        ProcessFile(inputFilePath, outputFilePath)

    timeN = time.time()
    durationMinutes = int((timeN - time0) / 60)
    print(f'Total Translation Minutes = {durationMinutes}')
