import os

inputFileRange = [*range(1, 41)]  # all files from 1-40
inputFileBaseName = 'shanghai-ccp-member'
inputDir = f'Data/FixedTranslatedPartials'.replace('/', os.path.sep)
outputDir = 'Data'
outputFileName = f'{inputFileBaseName}-translated.csv'

def MergeFiles(outputPath:str,*inputPaths:str):
    outputFile = open(outputPath, 'a', encoding='utf-8')
    for inputPath in inputPaths:
        for line in open(inputPath, 'r', encoding='utf-8'):
            outputFile.write(line)
    outputFile.close()

if __name__ == "__main__":
    outputPath = os.path.join(outputDir, outputFileName)
    if os.path.exists(outputPath):
        os.remove(outputPath)
    fileNames = []
    for fileNumber in inputFileRange:
        fileName = f'{inputFileBaseName}_{fileNumber}.csv'
        inputFilePath = os.path.join(inputDir, fileName)
        fileNames.append(inputFilePath)
    MergeFiles(outputPath, *fileNames)