import os


inputFileRange = [*range(1, 41)]  # all files from 1-40
inputFileBaseName = 'shanghai-ccp-member'
inputDir = f'Data/UntranslatedPartials'.replace('/', os.path.sep)
# outputDir = f'Data/TranslatedPartials'.replace('/', os.path.sep)
outputDir = f'Data/FixedTranslatedPartials'.replace('/', os.path.sep)


def GetRowCount(filePath:str) -> int:
    rows = 0
    with open(filePath, 'r', encoding='utf-8') as inputFile:
        for line in inputFile:
            if line.strip() != '':
                rows += 1
    return rows


def RowCountsMatch(inputFilePath:str, outputFilePath:str) -> bool:
    inputfileCount = GetRowCount(inputFilePath)
    outputFileCount = GetRowCount(outputFilePath)
    if inputfileCount == outputFileCount:
        return True
    else:
        return False

def GetColumnCount(inputRow:str) -> int:
    cnt = 0
    isQualified = False
    for t in inputRow:
        if t == ',' and isQualified == False:
            cnt += 1
        elif t == '"':
            if isQualified == False:
                isQualified = True
            else:
                isQualified = False 
        else:
            pass
    return cnt + 1

def GetTranslationErrors(inputFilePath:str, outputFilePath:str) -> int:
    with open(inputFilePath, 'r', encoding='utf-8') as inputFile:
        inputFileLines = inputFile.readlines()

    with open(outputFilePath, 'r', encoding='utf-8') as outputFile:
        outputFileLines = outputFile.readlines()

    errorsFound = 0
    n = 0
    while n < len(inputFileLines):
        outputLine = outputFileLines[n]
        if 'Error Translating Record' in outputLine:
            errorsFound += 1
            inputLine = inputFileLines[n]
            print(f'***Translation Error Encountered converting {inputFilePath} to {outputFilePath}***')
            print(inputLine)
            print(outputLine)
        if errorsFound:
            break
        n += 1
    return errorsFound

def RowColumnsMatch(inputFilePath:str, outputFilePath:str) -> bool:
    with open(inputFilePath, 'r', encoding='utf-8') as inputFile:
        inputFileLines = inputFile.readlines()

    with open(outputFilePath, 'r', encoding='utf-8') as outputFile:
        outputFileLines = outputFile.readlines()

    countsMatch = True
    n = 0
    while n < len(inputFileLines):
        inputLine = inputFileLines[n]
        outputLine = outputFileLines[n]
        inputColumnCount = GetColumnCount(inputLine)
        outputColumnCount = GetColumnCount(outputLine)
        if inputColumnCount != outputColumnCount:
            countsMatch = False
            print(f'***Column counts do not match when comparing {inputFilePath} to {outputFilePath}***')
            print(inputLine)
            print(outputLine)
        n += 1
    return countsMatch

if __name__ == '__main__':
    for fileNumber in inputFileRange:
        fileName = f'{inputFileBaseName}_{fileNumber}.csv'
        inputFilePath = os.path.join(inputDir, fileName)
        outputFilePath = os.path.join(outputDir, fileName)

        countsMatch = RowCountsMatch(inputFilePath, outputFilePath)
        columnCountsMatch = RowColumnsMatch(inputFilePath, outputFilePath)
        countTranslationErrors = GetTranslationErrors(inputFilePath, outputFilePath)

        print(f'{fileName} | Row Counts match = {countsMatch} | Column Counts match = {columnCountsMatch} | Translation Errors = {countTranslationErrors}')